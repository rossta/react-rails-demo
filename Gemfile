source "https://rubygems.org"
ruby "2.2.3"

gem "rails", "~>4.2"
gem "sqlite3", group: :development
gem "pg", group: :production

gem "reimagine2", github: "challengepost/reimagine", branch: "global_nav_with_configuration"
gem "compass-rails", "2.0.4" # http://stackoverflow.com/questions/30641655/rails-uninitialized-constant-sprocketssasscachestore
gem "uglifier", "~> 2.7.2"
gem "coffee-rails"

gem "turbolinks"
gem "jbuilder"
gem "sdoc", group: :doc
gem "es5-shim-rails"

# Use ActiveModel has_secure_password
# gem "bcrypt", "~> 3.1.7"

# Use Rails Html Sanitizer for HTML sanitization
gem "rails-html-sanitizer"

# Use Unicorn as the app server
gem "unicorn"

gem "react_on_rails", "~> 0.1.8"
gem "therubyracer", platforms: :ruby

gem "autoprefixer-rails"

gem "awesome_print"

# Use Capistrano for deployment
# gem "capistrano-rails", group: :development
group :production do
  gem "rails_12factor"
end

group :development, :test do
  # Access an IRB console on exceptions page and /console in development
  gem "web-console"

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem "spring"

  gem "spring-commands-rspec"

  # Manage application processes
  gem "foreman"

  gem "factory_girl_rails"

  gem "rubocop", require: false

  gem "ruby-lint", require: false

  gem "scss_lint", require: false

  gem "brakeman", require: false

  gem "bundler-audit", require: false

  gem "rainbow"

  gem "quiet_assets"

  # Favorite debugging gems
  gem "pry"
  gem "pry-doc"
  gem "pry-rails"
  gem "pry-stack_explorer"
  gem "pry-rescue"
  gem "pry-byebug"
end

group :test  do
  gem "coveralls", require: false
  gem "rspec-rails"
  gem "capybara"
  gem "capybara-screenshot"
  gem "selenium-webdriver"
  gem "chromedriver-helper"
  gem "database_cleaner"
  gem "launchy"
end
